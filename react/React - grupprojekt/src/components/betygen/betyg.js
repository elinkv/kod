import React, {Component} from 'react';
import './betyg.css';
import {Progress, Container, Col, Row} from 'reactstrap';

export default class Betyg extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      status: {},
      allCourses: {}
    };
  }
  componentDidMount() {
    fetch("https://popcornapi2.000webhostapp.com/index.php/?/" + this.props.urlClass + "/" + this.props.secret + "").then(res => res.json()).then((result) => {
      this.setState({
        isLoaded: true,
        progress: result.status,
        progress2: result.status.replace("%",""),
        allCourses: [result]
      });
      alert(this.state.progress2);
    }, (error) => {
      this.setState({error});
    }).then(fetch("https://popcornapi2.000webhostapp.com/index.php/?/fe16&kurser=true").then(res => res.json()).then((result2) => {

      var courses = [];

      //var self = this;
      console.log(this.state);
      result2.forEach((item, index) => {
        var extras = this.fetchGrades(item.Kurskod, this.state.allCourses[0]);
        courses.push(<ul className="t" key={index}>
          <h2>{item.Kurskod}
            .{item.Kursnamn}</h2>
          <div>
            <p>{extras}</p>
          </div>
        </ul>)
        console.log(extras)
      });
      this.setState({kursnamnen: courses});
    }, (error) => {
      this.setState({error});
    }))
  }

  fetchGrades = (kurskod, allCourses) => {

    var text = [];
    for (let key in allCourses) {
      let uppgift = key.split("\n");
      if (uppgift[0] == kurskod) {
        console.log(" kurs: " + kurskod + " uppgift:" + uppgift[1] + "betyg: " + allCourses[key] + " ");
        var grades = ("Uppgift: " + uppgift[1] + ": " + allCourses[key] + " ")
        text.push(grades);
      }
    }
    return text;
  }
  render() {
    const {error, isLoaded, status} = this.state;
    if (error) {
      return <div id='error-msg'>
        <h2>Koden ej giltlig...</h2>
      </div>;
    } else if (!isLoaded) {
      return <div className="loader"></div>;
    } else {
      return (<div className="betyg">
        <h2>Övergripande status</h2>
        <Progress value={this.state.progress2}>{this.state.progress}
        </Progress>
        <p>
          {this.state.kursnamnen}</p>
      </div>);
    }
  }
}
