import React, {Component} from 'react';
import './loginbox.css';
import { Form,FormGroup,Label,Input,FormFeedback,Button } from 'reactstrap';

export default class Loginbox extends Component {

  render() {
    return (<div className="loginbox">

      <FormGroup>
        <Form onSubmit={this.props.submitIt}>
        <Label for="exampleEmail"></Label>
        <Input placeholder="Personlig kod..." type="text" value={this.props.value} onChange={this.props.change} />
        <FormFeedback>You will not be able to see this</FormFeedback>
        <Button onClick={this.props.funk} type='submit'>Logga in</Button>
        {this.props.errorIs}
        </Form>
      </FormGroup>

    </div>);
  }
}

