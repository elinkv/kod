import React, {Component} from 'react';
import Chart from '../chart/chart.js';
import Loginbox from '../loginbox/loginbox.js'
import Betyg from '../betygen/betyg.js'
import {Container, Col, Row} from 'reactstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';



export default class App extends Component {
  constructor( props ){
      super( props )
      this.state = { show : false,
                     value: '',
                     error1: ''
                  };

      this.toggleDiv = this.toggleDiv.bind(this)
  }

  toggleDiv = () => {
      const { show } = this.state;
      if(this.state.value != ''){
        this.setState( { show : !show } )
      }else{
        //alert("Saknar personlig kod...");
        this.setState({error1: <div className='error-msg'>Kod ej angiven...</div>});
      }
  }

  handleChange = (event) => {
    this.setState({value: event.target.value});
  }

  handleSubmit = (event) => {
    event.preventDefault();
  }
  

  render() {
    return (<Container className="app">
      <Row>
        <Col xs="10">

          <Chart urlClass={window.location.pathname.replace(/^\/([^\/]*).*$/, '$1')} /></Col>
        <Col xs="2" className="login"><Loginbox funk = {this.toggleDiv} change={this.handleChange} submitIt={this.handleSubmit} errorIs={this.state.error1}>
</Loginbox>
        </Col>

      </Row>
      <Col xs="10" > { this.state.show && <Betyg urlClass={window.location.pathname.replace(/^\/([^\/]*).*$/, '$1')} secret={this.state.value} /> } </Col>
    </Container>) 
  }
}
