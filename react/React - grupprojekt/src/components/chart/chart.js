import React, {Component} from 'react';
import './chart.css';
import {
  BarChart,
  CartesianGrid,
  XAxis,
  YAxis,
  Tooltip,
  Legend,
  Bar,
  ResponsiveContainer
} from 'recharts';
// Fetchning av data för barcharten
export default class Chart extends Component {

  state = {
    chartData:[


  ],


};



getData = async (e) =>{
  let grades = new Array();
  let temp = {}
  const api_call = await fetch('https://popcornapi2.000webhostapp.com/index.php/?/' + this.props.urlClass);
  const response = await api_call.json();
  console.log(response);
  for(var key in response) {
    if(response.hasOwnProperty(key)) {
      temp = response[key];
      temp['name'] = key;
      grades.push(temp);
      temp.name = temp.name.replace(/\s/g, "\r\n");
    }
  }

  this.setState({
    chartData: grades
  });


}

componentWillMount(){
  this.getData();
}
  render() {
    return (
    <div className = "hejsan">
      <ResponsiveContainer width='100%' aspect={4.0/2.0} className = "chart">
      <BarChart width={2000} height={600} data={this.state.chartData} dataKey="data"  >
        <CartesianGrid strokeDasharray="3 3"/>
        <XAxis dataKey="name"/>
        <YAxis/>
        <Tooltip cursor={false} />
        <Legend/>
        <Bar dataKey="SK" stackId="a" fill="#A5C1F4" barSize={100}   barCategoryGap={100}/>
        <Bar dataKey="G" stackId="a" fill="#B7D9A8" barSize={100}/>
        <Bar dataKey="VG" stackId="a" fill="#95C77D" barSize={100}/>
        <Bar dataKey="K" stackId="a" fill="#f8cb9c" barSize={100}/>
        <Bar dataKey="IG" stackId="a" fill="#de6166" barSize={100}/>
      </BarChart>
    </ResponsiveContainer>
    </div>
  );
  }
}
