import React, {Component} from 'react';
import './grades.css';
import { Progress } from 'reactstrap';

  export default class Betyg extends Component {
  render() {
    var progress = 73;
    var betyg1 = "VG";
    var betyg2 = "G";
    return (

      <div className="betyg">
    <div>
    <h2>Övergripande status</h2>
<p>Så här ligger du till som helhet i utbildningen baserat på andelen examinationsmoment du är klar med</p>
 <Progress color="success" value="73"> {progress}%</Progress>
 <h2>Träffar</h2>
 <div className="wrapper">
    <div className="box">1</div>
    <div className="box">2</div>
    <div className="box">3</div>
     <div className="box">4</div>
     <div className="box">5</div>
     <div className="box">6</div>
</div>
<div className="tesst">
  <div className="box1">x</div>
  <div className="box1">x</div>
  <div className="box1"></div>
  <div className="box1"></div>
  <div className="box1"></div>
  <div className="box1"></div>
</div>
</div>
    <h2>Kurser</h2>
    <h4>1. Introduktion till digitala kommunikationskalaner</h4>
    <div className="wrapper">
      <div className="box">U1</div>
      <div className="box">U2</div>
      <div className="box">Tenta</div>
        <div className="box">Betyg</div>
</div>
    <div className="slutbetyg">
      <div className="box vg">{ betyg1 }</div>
      <div className="box g">{ betyg2 }</div>
      <div className="box g">{ betyg2 }</div>
      <div className="box g">{ betyg2 }</div>
</div>
    <h4>1. juridik inom digital medier</h4>
    <div className="wrapper">
      <div className="box">U1</div>
      <div className="box">Tenta</div>
      <div className="box">Betyg</div>

    </div>
    <div className="slutbetyg">
      <div className="box vg">{ betyg1 }</div>
      <div className="box vg">{ betyg2 }</div>
      <div className="box g">{ betyg2 }</div>
    </div>

   <h4>3. Digital kommunikation</h4>
    <div className="wrapper">
      <div className="box">U1</div>
      <div className="box">U2</div>
      <div className="box">U3</div>
      <div className="box">U4</div>
      <div className="box">U5</div>
      <div className="box">Betyg</div>
  </div>
    <div className="slutbetyg">
      <div className="box g">{ betyg2 }</div>
      <div className="box g">{ betyg2 }</div>
      <div className="box vg">{ betyg2 }</div>
      <div className="box g">{ betyg2 }</div>
      <div className="box g">{ betyg2 }</div>
      <div className="box g">{ betyg2 }</div>
</div>
    <h4>3. Presenations och paketering</h4>
    <div className="wrapper">
      <div className="box">U1</div>
      <div className="box">U2</div>
      <div className="box">Betyg</div>
</div>
     <div className="slutbetyg">
      <div className="box g">{ betyg2 }</div>
      <div className="box g">{ betyg2 }</div>
      <div className="box g">{ betyg2 }</div>
  </div>
</div>);
  }
}
