import React, { Component } from 'react';
import './Footer.css';

export default class Footer extends Component {
  render() {
    return (
      <footer><p id='footer-text'>Copyright © 2018 Elin Kviberg. All rights reserved.</p></footer>
    );
  }
}
