import React, { Component } from 'react';
import './Nav.css';
import Home from '../Home/Home.js';
import Guestbook from '../Guestbook/Guestbook.js';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import Footer from '../Footer/Footer.js';
import Contact from '../Contact/Contact.js';
import Weather from '../Weather/Weather.js';

export default class Nav extends Component {
  render() {

    const Links = () => (
        <Router>
            <div>
                <nav>
                    <ul>
                        <li><Link to="/">Hem</Link></li>
                        <li><Link to="/weather">Väder</Link></li>
                        <li><Link to="/guestbook">Gästbok</Link></li>
                        <li><Link to="/kontakt">Kontakt</Link></li>
                    </ul>
                </nav>
                <div>
                    <Route exact path="/" component={Home} />
                    <Route path="/weather" component={Weather} />
                    <Route path="/guestbook" component={Guestbook} />
                    <Route path="/kontakt" component={Contact} />
                </div>
                <Footer />
            </div>
        </Router>
      );

    return (
        <Router>
            <Links />
        </Router>
    );
  }
}
