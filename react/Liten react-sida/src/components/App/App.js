import React, { Component } from 'react';
import './App.css';
import Nav from '../Nav/Nav.js'

export default class App extends Component {
  render() {
    return (
      <div className="App">
        <Nav />
      </div>
    );
  }
}
