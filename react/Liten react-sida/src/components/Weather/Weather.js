import React, { Component } from 'react';
import './Weather.css';

export default class Home extends Component {
  constructor(props){
    super(props);

    this.state = {
      weather: {
          temp: undefined,
          pressure: 0,
          humidity: 0,
          temp_min: 0,
          temp_max: 0,
          celcius: 2
      },
      city: '-'
    }
  }
  
  handleSubmit = (event) => {
    fetch('http://api.openweathermap.org/data/2.5/weather?q=' + this.state.city + '&appid=29ee7084d7b62689497277032a7ae4d2')
    .then(response => response.json())
    .then(data => this.setState({weather: data.main}))
    event.preventDefault();
  }

  handleCityChange = (event) => {
    this.setState({city: event.target.value});
  }

  outputCelcius = () => {
    if(this.state.city != "-" ){
    return <p id='temp'><span>{Math.round(this.state.weather.temp-273.15)}</span> grader celcius</p>;
    }
  }

  render() {
    return (
      <div id='weather'>
        <div id='weather-holder'>
          <h1>Väder</h1>
          <form onSubmit={this.handleSubmit} >
              <input type='text' name='city' onChange={this.handleCityChange}  placeholder='Ange en stad'/>
              <button type='submit' value="Submit">ok</button>
          </form>
          <div id='result'>
          {this.outputCelcius()}
          </div>
        </div>
      </div>
    );
  }
}