import React, { Component } from 'react';
import './Guestbook.css';
import Post from './Post/Post.js'

export default class Guestbook extends Component {

  state = {name: '',
          message: '',
          comments: ''
        };

  componentDidMount(){
    this.fetchData();
  }      

  // for newly posted comments in guestbook
  componentDidUpdate(){
    this.fetchData();
  }

  handleNameChange= (event) => {
    this.setState({name: event.target.value});
  }

  handleMessageChange= (event) => {
    this.setState({message: event.target.value});
  }

  handleSubmit = (event) => {
    fetch('https://radiant-earth-96342.herokuapp.com/guestbook',
      {
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
          },
          method: "POST",
          body: JSON.stringify({name: this.state.name , message: this.state.message})
      })
      .then(function(res){ console.log(res) })
      .catch(function(res){ console.log(res) })
      this.setState({
                    name: '',
                    message: ''
                  });
      event.preventDefault();
  }

   outputComments = () => {
    var allComments = [];
    for(let i=0; i<Object.keys(this.state.comments).length; i++){
      allComments.push(<Post name={this.state.comments[i].name} message={this.state.comments[i].message} />);
    }
    return allComments;
  }

  fetchData = () => {
    fetch('https://radiant-earth-96342.herokuapp.com/guestbook')
    .then(response => response.json())
    .then(data => this.setState({ comments: data }));
  }


  render() {
    return (
      <div id='guestbook'>
      <h1>Gästbok</h1>
        <div id='gw'>
          <div id='guestbook-holder'>
            <form onSubmit={this.handleSubmit}>
              <input type="text" name='name' onChange={this.handleNameChange} value={this.state.name} placeholder='Namn' className='mb' required/>
              <textarea name='message' onChange={this.handleMessageChange} value={this.state.message} placeholder='mitt meddelande...' className='mb' required></textarea>
              <input type="submit" value="Submit" className='mb' />
            </form>
            {this.outputComments()} 
          </div>
        </div>
      </div>
    );
  }
}

