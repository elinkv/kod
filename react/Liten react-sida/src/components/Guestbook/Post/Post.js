import React, { Component } from 'react';
import './Post.css';

export default class Post extends Component {
  render() {
    return (
      <div className='post'>
        <h4>{this.props.name}</h4>
        <p className='post-message'>{this.props.message}</p>
      </div>
    );
  }
}


