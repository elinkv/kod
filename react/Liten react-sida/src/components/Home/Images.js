export default {
    htmlLogo: require('./Images/html.png'),
    cssLogo: require('./Images/css.png'),
    jsLogo: require('./Images/js.png'),
    phpLogo: require('./Images/php.png')
}