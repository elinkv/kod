import React, { Component } from 'react';
import './Home.css';
import Images from './Images'
let {htmlLogo, cssLogo, jsLogo, phpLogo} = Images

export default class Home extends Component {
  render() {
    return (
      <div id='om-mig'>
          <h1>Hej!</h1>
          <p className='about-text'>
          Lorem ipsum dolor sit amet, cum duis dolor gloriatur ne, ad his sapientem evertitur suscipiantur. 
          Te ius meis probo nonumy, eam no persius utroque. Te iuvaret sapientem voluptatum eum, petentium consequuntur 
          comprehensam no his, his at enim case contentiones. Graeci aliquip ne eam, no ius solum sadipscing. Te tollit graecis eum</p>

            <p className='about-text'>
            Tekniker jag jobbar med:
            </p>
            <div id='techniques'>
              <p className='b'><img src={htmlLogo} alt='html logga' id='htmlLogo' /><br /> HTML5</p>
              <p className='b'><img src={cssLogo} alt='css logga' id='cssLogo' /><br /> CSS3</p>
              <p className='b'><img src={jsLogo} alt='js logga' id='jsLogo' /><br /> Javascript</p>
              <p className='b'><img src={phpLogo} alt='php logga' id='phpLogo' /><br /> PHP</p>
            </div>
      </div>
    );
  }
}