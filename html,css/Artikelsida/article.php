<?php
include_once 'classes/ShowArticle.php';
include_once 'classes/Links.php';

$article = new ShowArticle();
$links = new Links();

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="description" content="<?php echo $article->meta_page_description; ?>">
  <meta name="keywords" content="<?php echo $article->meta_page_keywords; ?>">
  <title><?php echo $article->article_title; ?> - Name</title>
  <link rel="stylesheet" href="css/style.css">
  <meta content="width=device-width, initial-scale=1.0, user-scalable=0" name="viewport">
</head>
<body>
  <header>
    <nav id="desktop">
      <div>
        <ul id="nav_desktop">
          <li><a href="index.php" id="logo_text">Name</a></li>
          <li><a href="index.php">Home</a></li>
          <li><a href="category.php">All articles</a></li>
          <li><a href="category.php?c=Economy">Economy</a></li>
          <li><a href="category.php?c=Other">Other</a></li>
        </ul>
        <ul>
          <li onclick="showSearchBox(); return 0;" onmouseover="colorChange()" onmouseleave="colorChangeBack()" id="srch_n"><img src="img/search-w.png" id="search_it"></li>
          <li id="menu_icon" onclick="mobileMenu(); return 0;"><img src="img/menu.png"></li>
        </ul>
      </div>
    </nav>
    <div id="important_message"></div>
    <div id="search_container">
      <div id="search_it">
        <form class="search_form">
          <input type="text" name="" id="search_input" onkeypress="return searchKeyPress(event);">
          <button type="button" name="ts" onclick="get_search();" id="search_button">Search</button>
        </form>
      </div>
    </div>
    <nav id="mobile">
      <div>
        <ul>
          <li><a href="index.php">Home</a></li>
          <li><a href="category.php">All articles</a></li>
          <li><a href="category.php?c=Economy">Economy</a></li>
          <li><a href="category.php?c=Other">Other</a></li>
        </ul>
      </div>
    </nav>
    <div id="banner_ad_container">
      <div></div>
    </div>
  </header>
  <main id="article">
    <article>
      <h1 class="align-self-c margin-b-10"><?php echo $article->article_title; ?></h1>
      <img class="align-self-c" src="<?php echo '' . $article->article_image_name . ''; ?>" alt="<?php echo $article->article_image_alt; ?>">
      <br>
      <?php echo $article->part_1; ?>
      <!--<div class="article_ad"><img src="https://lh6.ggpht.com/Ab63gyKVnGwbQaj0guyJ0caGj-VugefmMd3SyzpPOX2RgCDA1tzQTY36sGI65Guw5OXdas4f5w=w303"></div>-->
      <?php echo $article->part_2; ?>
      <!--< <div class="article_ad"><img src="https://lh6.ggpht.com/Ab63gyKVnGwbQaj0guyJ0caGj-VugefmMd3SyzpPOX2RgCDA1tzQTY36sGI65Guw5OXdas4f5w=w303"></div>-->
      <?php echo $article->part_3; ?>
      <br>
      <br>
      <h5 class="align-self-c">Share this article</h5>
      <div id="share_container" class="align-self-c">
        <a href="#"><img src="img/fb.png"></a>
        <a href="#"><img src="img/twitter.png"></a>
        <a href="#"><img src="img/google-plus.png"></a>
      </div>
      <h2 class="align-self-c-mobile margin-t-10">Related articles</h2>
      <ul class="related_articles align-self-c-mobile">
        <?php $article->get_related_articles();?>
      </ul>
    </article>
    <aside>
      <h3>Newest articles</h3>
      <div class="subject_l_container">
        <div></div>
      </div>
      <ul>
        <?php $links->get_10_newest_articles();?>
      </ul>
      <h3 class="margin-t-10">Other articles</h3>
      <div class="subject_l_container">
        <div></div>
      </div>
      <ul>
        <?php $links->get_10_random_articles();?>
      </ul>
    </aside>
  </main>
  <footer>
    <ul>
      <li><a href="about.php">About us</a></li>
      <li><a href="contact.php">Contact</a></li>
      <li><a href="terms-of-use.php">Terms of use</a></li>
      <li><a href="privacy-policy.php">Privacy Policy</a></li>
    </ul>
    <p>© Copyright 2017. All Rights Reserved.</p>
  </footer>

  <noscript>This website works best with JavaScript enabled</noscript>
  <script type="text/javascript" src="js/search.js"></script>
  <script type="text/javascript" src="js/mobileMenu.js"></script>

</body>
</html>

