<?php
include_once 'config/db.php';
class indexArticles {

	// TODO: fixa finare kod...

	public $newest_1_article_title = null;
	public $newest_1_article_image_name = null;
	public $newest_1_article_image_alt = null;
	public $newest_1_page_link = null;
	public $newest_2_article_image_name = null;
	public $newest_2_article_image_alt = null;
	public $newest_2_article_title = null;
	public $newest_2_page_link = null;
	public $newest_3_article_title = null;
	public $newest_3_article_image_name = null;
	public $newest_3_article_image_alt = null;
	public $newest_3_page_link = null;
	public $random_1_article_title = null;
	public $random_1_article_image_name = null;
	public $random_1_article_image_alt = null;
	public $random_1_page_link = null;
	public $random_1_category = null;
	public $random_2_article_title = null;
	public $random_2_article_image_name = null;
	public $random_2_article_image_alt = null;
	public $random_2_page_link = null;
	public $random_2_category = null;
	public $random_3_article_title = null;
	public $random_3_article_image_name = null;
	public $random_3_article_image_alt = null;
	public $random_3_page_link = null;
	public $random_3_category = null;
	public $random_4_article_title = null;
	public $random_4_article_image_name = null;
	public $random_4_article_image_alt = null;
	public $random_4_page_link = null;
	public $random_4_category = null;
	public $category_1 = null;
	public $category_2 = null;
	public $category_1_article_1_title = null;
	public $category_1_article_1_image_name = null;
	public $category_1_article_1_image_alt = null;
	public $category_1_page_link_1 = null;
	public $category_1_article_2_title = null;
	public $category_1_article_2_image_name = null;
	public $category_1_article_2_image_alt = null;
	public $category_1_page_link_2 = null;
	public $category_1_article_3_title = null;
	public $category_1_article_3_image_name = null;
	public $category_1_article_3_image_alt = null;
	public $category_1_page_link_3 = null;
	public $category_1_article_4_title = null;
	public $category_1_article_4_image_name = null;
	public $category_1_article_4_image_alt = null;
	public $category_1_page_link_4 = null;
	public $category_1_article_5_title = null;
	public $category_1_article_5_image_name = null;
	public $category_1_article_5_image_alt = null;
	public $category_1_page_link_5 = null;
	public $category_1_article_6_title = null;
	public $category_1_article_6_image_name = null;
	public $category_1_article_6_image_alt = null;
	public $category_1_page_link_6 = null;
	public $category_1_article_7_title = null;
	public $category_1_article_7_image_name = null;
	public $category_1_article_7_image_alt = null;
	public $category_1_page_link_7 = null;
	public $category_1_article_8_title = null;
	public $category_1_article_8_image_name = null;
	public $category_1_article_8_image_alt = null;
	public $category_1_page_link_8 = null;
	public $meta_page_title = null;
	public $meta_page_description = null;
	public $meta_page_keywords = null;
	public $article_category = null;
	public $article_sub_category = null;
	public $part_1 = null;
	public $part_2 = null;
	public $part_3 = null;

	public function __construct() {

		$this->get_article_info_newest_1();
		$this->get_article_info_newest_2();
		$this->get_article_info_newest_3();
		$this->get_4_random_articles();
		$this->get_2_dif_categorie_articles();
	}

	function get_article_info_newest_1() {

		$this->db_connection = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

		if (!$this->db_connection->set_charset("utf8")) {
			$this->errors[] = $this->db_connection->error;
		}

		$sql = "SELECT article_title, part_1, part_2, part_3, article_image_name, article_image_alt, page_link, meta_page_title, meta_page_description, meta_page_keywords, category, sub_category
	  FROM articles ORDER BY added desc limit 1";
	  
		$query = $this->db_connection->query($sql);
		$row = $query->fetch_assoc();

		$this->newest_1_article_title = $row['article_title'];
		$this->newest_1_article_image_name = $row['article_image_name'];
		$this->newest_1_article_image_alt = $row['article_image_alt'];
		$this->newest_1_page_link = $row['page_link'];
	}

	function get_article_info_newest_2() {

		$this->db_connection = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

		if (!$this->db_connection->set_charset("utf8")) {
			$this->errors[] = $this->db_connection->error;
		}

		$sql = "SELECT article_title, part_1, part_2, part_3, article_image_name, article_image_alt, page_link, meta_page_title, meta_page_description, meta_page_keywords, category, sub_category
	  FROM articles ORDER BY added desc limit 1, 2";
	  
		$query = $this->db_connection->query($sql);
		$row = $query->fetch_assoc();

		$this->newest_2_article_title = $row['article_title'];
		$this->newest_2_article_image_name = $row['article_image_name'];
		$this->newest_2_article_image_alt = $row['article_image_alt'];
		$this->newest_2_page_link = $row['page_link'];
	}

	function get_article_info_newest_3() {

		$this->db_connection = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

		if (!$this->db_connection->set_charset("utf8")) {
			$this->errors[] = $this->db_connection->error;
		}

		$sql = "SELECT article_title, part_1, part_2, part_3, article_image_name, article_image_alt, page_link, meta_page_title, meta_page_description, meta_page_keywords, category, sub_category
	  FROM articles ORDER BY added desc limit 2, 3";
	  
		$query = $this->db_connection->query($sql);
		$row = $query->fetch_assoc();

		$this->newest_3_article_title = $row['article_title'];
		$this->newest_3_article_image_name = $row['article_image_name'];
		$this->newest_3_article_image_alt = $row['article_image_alt'];
		$this->newest_3_page_link = $row['page_link'];
	}

	function get_4_random_articles() {

		$firstId = 1;
		$lastId = 4;
		$id_1 = rand($firstId, $lastId);

		while (in_array(($id_2 = rand($firstId, $lastId)), array($id_1)));
		while (in_array(($id_3 = rand($firstId, $lastId)), array($id_1, $id_2)));
		while (in_array(($id_4 = rand($firstId, $lastId)), array($id_1, $id_2, $id_3)));

		$this->db_connection = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

		if (!$this->db_connection->set_charset("utf8")) {
			$this->errors[] = $this->db_connection->error;
		}

		$sql1 = "SELECT article_title, part_1, part_2, part_3, article_image_name, article_image_alt, page_link, meta_page_title, meta_page_description, meta_page_keywords, category, sub_category
	  FROM articles WHERE id = $id_1";
	  
		$query1 = $this->db_connection->query($sql1);
		$row1 = $query1->fetch_assoc();

		$this->random_1_article_title = $row1['article_title'];
		$this->random_1_article_image_name = $row1['article_image_name'];
		$this->random_1_article_image_alt = $row1['article_image_alt'];
		$this->random_1_page_link = $row1['page_link'];
		$this->random_1_category = $row1['category'];

		$sql2 = "SELECT article_title, part_1, part_2, part_3, article_image_name, article_image_alt, page_link, meta_page_title, meta_page_description, meta_page_keywords, category, sub_category
	  FROM articles WHERE id = $id_2";
	  
		$query2 = $this->db_connection->query($sql2);
		$row2 = $query2->fetch_assoc();

		$this->random_2_article_title = $row2['article_title'];
		$this->random_2_article_image_name = $row2['article_image_name'];
		$this->random_2_article_image_alt = $row2['article_image_alt'];
		$this->random_2_page_link = $row2['page_link'];
		$this->random_2_category = $row2['category'];

		$sql3 = "SELECT article_title, part_1, part_2, part_3, article_image_name, article_image_alt, page_link, meta_page_title, meta_page_description, meta_page_keywords, category, sub_category
	  FROM articles WHERE id = $id_3";
	  
		$query3 = $this->db_connection->query($sql3);
		$row3 = $query3->fetch_assoc();
		$this->random_3_article_title = $row3['article_title'];
		$this->random_3_article_image_name = $row3['article_image_name'];
		$this->random_3_article_image_alt = $row3['article_image_alt'];
		$this->random_3_page_link = $row3['page_link'];
		$this->random_3_category = $row3['category'];

		$sql4 = "SELECT article_title, part_1, part_2, part_3, article_image_name, article_image_alt, page_link, meta_page_title, meta_page_description, meta_page_keywords, category, sub_category
	  FROM articles WHERE id = $id_4";
	  
		$query4 = $this->db_connection->query($sql4);
		$row4 = $query4->fetch_assoc();
		$this->random_4_article_title = $row4['article_title'];
		$this->random_4_article_image_name = $row4['article_image_name'];
		$this->random_4_article_image_alt = $row4['article_image_alt'];
		$this->random_4_page_link = $row4['page_link'];
		$this->random_4_category = $row4['category'];
	}

	function get_2_dif_categorie_articles() {

		$categories = array("Other", "Economy");
		$amound_of_categories = count($categories);
		$firstcat = 0;
		$lastcat = $amound_of_categories - 1;
		$cat_1 = rand($firstcat, $lastcat);
		$this->category_1 = $categories[$cat_1];
		
		while (in_array(($cat_2 = rand($firstcat, $lastcat)), array($cat_1)));
		$this->category_2 = $categories[$cat_2];
		$this->db_connection = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
		if (!$this->db_connection->set_charset("utf8")) {
			$this->errors[] = $this->db_connection->error;
		}

		$sql = "SELECT article_title, part_1, part_2, part_3, article_image_name, article_image_alt, page_link, meta_page_title, meta_page_description, meta_page_keywords, category, sub_category
	  FROM articles WHERE category = ('$categories[$cat_1]') ORDER BY added desc limit 1";
	  
		$query = $this->db_connection->query($sql);
		$row = $query->fetch_assoc();

		$this->category_1_article_1_title = $row['article_title'];
		$this->category_1_article_1_image_name = $row['article_image_name'];
		$this->category_1_article_1_image_alt = $row['article_image_alt'];
		$this->category_1_page_link_1 = $row['page_link'];

		$sql2 = "SELECT article_title, part_1, part_2, part_3, article_image_name, article_image_alt, page_link, meta_page_title, meta_page_description, meta_page_keywords, category, sub_category
	  FROM articles WHERE category = ('$categories[$cat_1]') ORDER BY added desc limit 1, 2";
	  
		$query2 = $this->db_connection->query($sql2);
		$row2 = $query2->fetch_assoc();
		$this->category_1_article_2_title = $row2['article_title'];
		$this->category_1_article_2_image_name = $row2['article_image_name'];
		$this->category_1_article_2_image_alt = $row2['article_image_alt'];
		$this->category_1_page_link_2 = $row2['page_link'];

		$sql3 = "SELECT article_title, part_1, part_2, part_3, article_image_name, article_image_alt, page_link, meta_page_title, meta_page_description, meta_page_keywords, category, sub_category
	  FROM articles WHERE category = ('$categories[$cat_1]') ORDER BY added desc limit 2, 3";
	  
		$query3 = $this->db_connection->query($sql3);
		$row3 = $query3->fetch_assoc();

		$this->category_1_article_3_title = $row3['article_title'];
		$this->category_1_article_3_image_name = $row3['article_image_name'];
		$this->category_1_article_3_image_alt = $row3['article_image_alt'];
		$this->category_1_page_link_3 = $row3['page_link'];

		$sql4 = "SELECT article_title, part_1, part_2, part_3, article_image_name, article_image_alt, page_link, meta_page_title, meta_page_description, meta_page_keywords, category, sub_category
	  FROM articles WHERE category = ('$categories[$cat_1]') ORDER BY added desc limit 3, 4";
	  
		$query4 = $this->db_connection->query($sql4);
		$row4 = $query4->fetch_assoc();

		$this->category_1_article_4_title = $row4['article_title'];
		$this->category_1_article_4_image_name = $row4['article_image_name'];
		$this->category_1_article_4_image_alt = $row4['article_image_alt'];
		$this->category_1_page_link_4 = $row4['page_link'];

		$sql5 = "SELECT article_title, part_1, part_2, part_3, article_image_name, article_image_alt, page_link, meta_page_title, meta_page_description, meta_page_keywords, category, sub_category
	  FROM articles WHERE category = ('$categories[$cat_2]') ORDER BY added desc limit 1";
	  
		$query5 = $this->db_connection->query($sql5);
		$row5 = $query5->fetch_assoc();

		$this->category_1_article_5_title = $row5['article_title'];
		$this->category_1_article_5_image_name = $row5['article_image_name'];
		$this->category_1_article_5_image_alt = $row5['article_image_alt'];
		$this->category_1_page_link_5 = $row5['page_link'];

		$sql6 = "SELECT article_title, part_1, part_2, part_3, article_image_name, article_image_alt, page_link, meta_page_title, meta_page_description, meta_page_keywords, category, sub_category
	  FROM articles WHERE category = ('$categories[$cat_2]') ORDER BY added desc limit 1, 2";
	  
		$query6 = $this->db_connection->query($sql6);
		$row6 = $query6->fetch_assoc();

		$this->category_1_article_6_title = $row6['article_title'];
		$this->category_1_article_6_image_name = $row6['article_image_name'];
		$this->category_1_article_6_image_alt = $row6['article_image_alt'];
		$this->category_1_page_link_6 = $row6['page_link'];

		$sql7 = "SELECT article_title, part_1, part_2, part_3, article_image_name, article_image_alt, page_link, meta_page_title, meta_page_description, meta_page_keywords, category, sub_category
	  FROM articles WHERE category = ('$categories[$cat_2]') ORDER BY added desc limit 2, 3";
	  
		$query7 = $this->db_connection->query($sql7);
		$row7 = $query7->fetch_assoc();

		$this->category_1_article_7_title = $row7['article_title'];
		$this->category_1_article_7_image_name = $row7['article_image_name'];
		$this->category_1_article_7_image_alt = $row7['article_image_alt'];
		$this->category_1_page_link_7 = $row7['page_link'];

		$sql8 = "SELECT article_title, part_1, part_2, part_3, article_image_name, article_image_alt, page_link, meta_page_title, meta_page_description, meta_page_keywords, category, sub_category
	  FROM articles WHERE category = ('$categories[$cat_2]') ORDER BY added desc limit 3, 4";
	  
		$query8 = $this->db_connection->query($sql8);
		$row8 = $query8->fetch_assoc();
		
		$this->category_1_article_8_title = $row8['article_title'];
		$this->category_1_article_8_image_name = $row8['article_image_name'];
		$this->category_1_article_8_image_alt = $row8['article_image_alt'];
		$this->category_1_page_link_8 = $row8['page_link'];
	}
}
?>
