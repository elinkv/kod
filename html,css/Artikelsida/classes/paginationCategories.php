<?php
include_once 'config/db.php';
class paginationCategories {

	public $total_records;
	public $total_pages;

	function total_pages_and_records() {

		$this->db_connection = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

		//get current category from the url parameter "c"
		if (isset($_GET["c"])) {$category_url = $_GET["c"];} else { $category_url = "";};

		//replace url parameter category _ with ""
		$category_url_ready = str_replace('_', ' ', $category_url);
		if ($category_url != "") {
			$category = "WHERE (category = '" . $category_url_ready . "')";
		} else {
			$category = "";
		}

		$sql = "SELECT COUNT(*) FROM articles {$category}";
		if (!$sql) {
			echo "Something failed";
		}

		$result = $this->db_connection->query($sql);
		if ($result === FALSE) {
			die(); // TODO: better error handling
		}

		$num_rec_per_page = 20;
		$rows = mysqli_fetch_row($result);
		$total_records = $rows[0];
		$total_pages = ceil($total_records / $num_rec_per_page);
		$this->total_pages = $total_pages;
		$this->total_records = $total_records;
	}

	function outputPagination() {

		$this->total_pages_and_records();
		$total_pages = $this->total_pages;

		if (isset($_GET["p"]) != "") {$page = $_GET["p"];} elseif (isset($_GET['p']) == "") {$page = 1;} else { $page = 1;};
		if ($page == "") {
			$page = 1;
		} elseif (preg_match('#[^0-9]#', $page) == true) {
			$page = 1;
		}

		$next_page = $page + 1;
		$last_page = $page - 1;

		//get current category from the url parameter "c"
		if (isset($_GET["c"])) {$category_url = $_GET["c"];} else { $category_url = "";};
		if ($category_url != "") {
			$category_url_link = "$category_url";
		} else {
			$category_url_link = "";
		}

		if ($total_pages > 0 && $page <= $total_pages && preg_match('#[^0-9]#', $page) == false) {
			if ($this->total_records > 20) {
				if ($page != 1) {
					echo "<li><a href='category.php?p=$last_page&c=$category_url_link'>" . '« prev' . "</a></li> "; // Goto 1st page
				}
				
				$total_pages_minus_one = $total_pages - 1;
				switch ($page) {
				case 1:
					$start = 1;
					$end = min(1 + 3, $total_pages);
					break;
				case $total_pages:
					$start = max($page - 3, 1);
					$end = $page;
					break;
				default:
					if ($total_pages != 3) {
						if ($page != $total_pages_minus_one && $page > 1 && $total_pages != 3) {
							$start = $page - 1;
							$end = $page + 2;
						} elseif ($page == $total_pages_minus_one) {
							$start = $page - 2;
							$end = $page + 1;
						}
					} else {
						$start = $page - 1;
						$end = $page + 1;
					}
				}
				for ($i = $start; $i <= $end; $i++) {
					if ($i != $page) {
						echo "<li><a href='category.php?p=$i&c=$category_url_link'>" . $i . "</a> </li>";
					} elseif ($i == $page) {
						echo "<li class='active'><a href='category.php?p=$i&c=$category_url_link''>" . $i . "</a> </li>";
					}
				}
				if ($page == $total_pages) {
					echo "<li><a href='category.php?c=$category_url_link'>" . 'first ' . "</a></li> "; // Goto first page
				} elseif ($page != $total_pages) {
					echo "<li><a href='category.php?p=$next_page&c=$category_url_link'>" . 'next »' . "</a></li> "; // Goto next page
					echo "<li><a href='category.php?p=$total_pages&c=$category_url_link'>" . 'last' . "</a></li> "; // Goto first page
				}
			}
		} else {
			echo "No articles was found in this category. <a href='category.php'>show all articles</a></li>";
		}
	}
}
?>
