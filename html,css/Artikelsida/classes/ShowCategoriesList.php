<?php
class ShowCategoriesList {

	function outputPagination() {
		
		$total_pages = $this->getTotal_pages_();
		$next_page = $page + 1;
		$last_page = $page - 1;
		if ($total_pages > 0 && $page <= $total_pages && preg_match('#[^0-9]#', $page) == false) {
			if ($this->getTotal_records_() > 50) {
				if ($page != 1) {
					echo "<a href='index.php?ts={$text_search_url}&p=$last_page&ca=" . $url_category . "&c=" . $url_city . "&o=" . $url_omrade . "&s=" . $sortList . "'>" . '« Förra sidan' . "</a> "; // Goto 1st page
				}
				$total_pages_minus_one = $total_pages - 1;
				switch ($page) {
				case 1:
					$start = 1;
					$end = min(1 + 3, $total_pages);
					break;
				case $total_pages:
					$start = max($page - 3, 1);
					$end = $page;
					break;
				default:
					if ($total_pages != 3) {
						if ($page != $total_pages_minus_one && $page > 1 && $total_pages != 3) {
							$start = $page - 1;
							$end = $page + 2;
						} elseif ($page == $total_pages_minus_one) {
							$start = $page - 2;
							$end = $page + 1;
						}
					} else {
						$start = $page - 2;
						$end = $page + 1;
					}
				}
				for ($i = $start; $i <= $end; $i++) {
					if ($i != $page) {
						echo "<a href='index.php?ts={$text_search_url}&p=" . $i . "&ca=" . $url_category . "&c=" . $url_city . "&o=" . $url_omrade . "&s=" . $sortList . "'>" . $i . "</a> ";
					} elseif ($i == $page) {
						echo "<a class='active' href='index.php?ts={$text_search_url}&p=" . $i . "&ca=" . $url_category . "&c=" . $url_city . "&o=" . $url_omrade . "&s=" . $sortList . "'>" . $i . "</a> ";
					}
				}
				if ($page == $total_pages) {
					echo "<a href='index.php?ts={$text_search_url}&p=1&ca=" . $url_category . "&c=" . $url_city . "&o=" . $url_omrade . "&s=" . $sortList . "'>" . 'Första sidan' . "</a> "; // Goto first page
				} elseif ($page != $total_pages) {
					echo "<a href='index.php?ts={$text_search_url}&p=$next_page&ca=" . $url_category . "&c=" . $url_city . "&o=" . $url_omrade . "&s=" . $sortList . "'>" . 'Nästa sida »' . "</a> "; // Goto next page
					echo "<a href='index.php?ts={$text_search_url}&p=$total_pages&ca=" . $url_category . "&c=" . $url_city . "&o=" . $url_omrade . "&s=" . $sortList . "'>" . 'Sista sidan' . "</a> "; // Goto first page
				}
			}
		} else {
			echo "<div id='no_ads_found_container'><div id='no_ads_found_subject'>Inga annonser funna</div><div id='no_ads_text'>Detta beror på att de angivna sökalternativen inte stämmer överens med någon annons.ölöko
						<br>
						Prova att ändra dina sökalternativ
						<br>
						<br>
						Visa Sveriges <form action='index.php' style='display: inline;'><button>alla annonser</button></form></div></div>";
		}
	}
}
?>
