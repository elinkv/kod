<?php
include_once 'config/db.php';
class ShowArticle {

	public $article_title = null;
	public $part_1 = null;
	public $part_2 = null;
	public $part_3 = null;
	public $article_image_name = null;
	public $article_image_alt = null;
	public $article_category = null;
	public $article_sub_category = null;
	public $page_link = null;
	public $meta_page_title = null;
	public $meta_page_description = null;
	public $meta_page_keywords = null;

	public function __construct() {
		$this->getArticleInfo();
	}

	function get_current_article_link() {

		$article_link = htmlspecialchars($_GET["a"]);
		return $article_link;
	}

	function getArticleInfo() {

		$this->db_connection = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

		if (!$this->db_connection->set_charset("utf8")) {
			$this->errors[] = $this->db_connection->error;
		}

		$url_article_link = $this->get_current_article_link();
		if($url_article_link == null || $url_article_link == ""){
			header("Location: index.php");
			die();
		}

		$sql = "SELECT article_title, part_1, part_2, part_3, article_image_name, article_image_alt, added, page_link, meta_page_title, meta_page_description, meta_page_keywords, category, sub_category
	  FROM articles WHERE page_link = '" . $url_article_link . "'";
	  
		$query = $this->db_connection->query($sql);
		$row = $query->fetch_assoc();

		$this->article_title = $row['article_title'];
		$this->part_1 = $row['part_1'];
		$this->part_2 = $row['part_2'];
		$this->part_3 = $row['part_3'];
		$this->article_image_name = $row['article_image_name'];
		$this->article_image_alt = $row['article_image_alt'];
		$this->article_category = $row['category'];
		$this->article_sub_category = $row['sub_category'];
		$this->page_link = $row['page_link'];
		$this->meta_page_title = $row['meta_page_title'];
		$this->meta_page_description = $row['meta_page_description'];
		$this->meta_page_keywords = $row['meta_page_keywords'];
	}

	function get_related_articles() {

		$this->db_connection = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

		if (!$this->db_connection->set_charset("utf8")) {
			$this->errors[] = $this->db_connection->error;
		}

		$url_article_link = $this->get_current_article_link();

		$sql1 = "SELECT article_title, part_1, part_2, part_3, article_image_name, article_image_alt, added, page_link, meta_page_title, meta_page_description, meta_page_keywords, category, sub_category
	  FROM articles WHERE page_link = '" . $url_article_link . "'";
	  
		$query1 = $this->db_connection->query($sql1);
		$row1 = $query1->fetch_assoc();

		$article_category = $row1['category'];

		$sql = "SELECT article_title, part_1, part_2, part_3, article_image_name, article_image_alt, added, page_link, meta_page_title, meta_page_description, meta_page_keywords, category, sub_category
	  FROM articles WHERE category = '" . $article_category . "'";
	  
		$query = $this->db_connection->query($sql);
		while ($row = $query->fetch_assoc()) {
			
			$article_title = $row['article_title'];
			$article_category = $row['category'];
			$article_sub_category = $row['sub_category'];
			$page_link = $row['page_link'];
			
			echo "<li><a href='article.php?a=" . $page_link . "'>" . $article_title . "</a></li>";
		}
	}
}
?>
