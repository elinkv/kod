﻿<?php
include_once 'config/db.php';
class Search {

	protected $string;
	public $total_records;
	public $total_pages;

	function output_all_articles_based_on_search_text() {

		$this->db_connection = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

		if (!$this->db_connection->set_charset("utf8")) {
			$this->errors[] = $this->db_connection->error;
		}

		if (!$this->db_connection->connect_errno) {

			$num_rec_per_page = 20;

			//get current page from the url parameter "p"
			if (isset($_GET["p"]) != "") {$page = $_GET["p"];} elseif (isset($_GET["p"]) == "") {$page = 1;} else { $page = 1;};
			if (isset($_GET["ts"])) {$text_search_url = $_GET["ts"];} else { $text_search_url = "";};

			//replace _ with " "
			$text_search_it = str_replace('_', ' ', $text_search_url);

			//count number of words in the text search
			$number_of_search_it_words = str_word_count($text_search_it);

			//insert every tex search word into variables
			$words = explode(" ", $text_search_it);

			//word[0]
			//word[1]
			//make a string out of the words
			$word_arr = array();
			// fix current page based on what the url parameter "p" contains
			if ($page == "") {
				$page = 1;
			} elseif (preg_match('#[^0-9]#', $page) == true) {
				$page = 1;
			}

			$start_from = ($page - 1) * $num_rec_per_page;
			$sql = "SELECT * FROM articles WHERE article_title LIKE '%$text_search_it%' LIMIT {$start_from}, {$num_rec_per_page}";
			$ds = $this->db_connection->query($sql) or die($this->db_connection->error);

			while ($row = $ds->fetch_assoc()) {

				$article_title = $row['article_title'];
				$category_db = $row['category'];
				$article_image_name = $row['article_image_name'];
				$page_link = $row['page_link'];
				$article_image_alt = $row['article_image_alt'];
				//html for DB data-loop
				?>
				 <div class="search_item">
          			<div class="list_img"><a href="<?php echo 'article.php?a=' . $page_link . ''; ?>"><img src="<?php echo '' . $article_image_name . ''; ?>" alt="<?php echo $article_image_alt; ?>"></a></div>
          			<div class="list_info">
            			<h5><a href="<?php echo 'article.php?a=' . $page_link . ''; ?>" class="list_item_link black_link"><?php echo $article_title; ?></a></h5>
            			<h6><a href="<?php echo 'category.php?c=' . $category_db . ''; ?>"><?php echo $category_db; ?></a></h6>
          			</div>
        		</div>
            <?php
			}
		} else {
			$this->errors[] = "Something went very very wrong.";
			exit();
		}
	}

	function get_search_text() {

		//get current category from the url parameter "c"
		if (isset($_GET["ts"])) {$text = $_GET["ts"];} else { $text = "asd";};

		//replace url parameter category _ with ""
		$text_ready = str_replace('_', ' ', $text);

		return $text_ready;
	}

	function total_pages_and_records() {

		//get current category from the url parameter "c"
		if (isset($_GET["ts"])) {$text = $_GET["ts"];} else { $text = "";};

		//replace url parameter category _ with ""
		$text_ready = str_replace('_', ' ', $text);
		if ($text != "") {
			$text_search = "WHERE (article_title LIKE '%" . $text_ready . "%')";
		} else {
			$text_search = "";
		}

		$sql = "SELECT COUNT(*) FROM articles {$text_search}";

		if (!$sql) {
			echo "Something failed";
		}

		$result = $this->db_connection->query($sql);
		if ($result === FALSE) {
			die(); 
		}

		$num_rec_per_page = 20;
		$rows = mysqli_fetch_row($result);
		$total_records = $rows[0];
		$total_pages = ceil($total_records / $num_rec_per_page);
		$this->total_pages = $total_pages;
		$this->total_records = $total_records;
	}

	function outputPagination() {

		$this->total_pages_and_records();
		$total_pages = $this->total_pages;

		if (isset($_GET["p"]) != "") {$page = $_GET["p"];} elseif (isset($_GET['p']) == "") {$page = 1;} else { $page = 1;};
		if ($page == "") {
			$page = 1;
		} elseif (preg_match('#[^0-9]#', $page) == true) {
			$page = 1;
		}

		$next_page = $page + 1;
		$last_page = $page - 1;

		//get current category from the url parameter "c"
		if (isset($_GET["ts"])) {$text = $_GET["ts"];} else { $text = "";};
		if ($text != "") {
			$text_url_link = "$text";
		} else {
			$text_url_link = "";
		}
		
		if ($total_pages > 0 && $page <= $total_pages && preg_match('#[^0-9]#', $page) == false) {
			if ($this->total_records > 20) {
				if ($page != 1) {
					echo "<li><a href='search.php?p=$last_page&ts=$text_url_link'>" . '« prev' . "</a></li> "; // Goto 1st page
				}
				$total_pages_minus_one = $total_pages - 1;
				switch ($page) {
				case 1:
					$start = 1;
					$end = min(1 + 3, $total_pages);
					break;
				case $total_pages:
					$start = max($page - 3, 1);
					$end = $page;
					break;
				default:
					if ($total_pages != 3) {
						if ($page != $total_pages_minus_one && $page > 1 && $total_pages != 3) {
							$start = $page - 1;
							$end = $page + 2;
						} elseif ($page == $total_pages_minus_one) {
							$start = $page - 2;
							$end = $page + 1;
						}
					} else {
						$start = $page - 1;
						$end = $page + 1;
					}
				}
				for ($i = $start; $i <= $end; $i++) {
					if ($i != $page) {
						echo "<li><a href='search.php?p=$i&ts=$text_url_link'>" . $i . "</a> </li>";
					} elseif ($i == $page) {
						echo "<li class='active'><a href='search.php?p=$i&ts=$text_url_link''>" . $i . "</a> </li>";
					}
				}
				if ($page == $total_pages) {
					echo "<li><a href='search.php?ts=$text_url_link'>" . 'first ' . "</a></li> "; // Goto first page
				} elseif ($page != $total_pages) {
					echo "<li><a href='search.php?p=$next_page&ts=$text_url_link'>" . 'next »' . "</a></li> "; // Goto next page
					echo "<li><a href='search.php?p=$total_pages&ts=$text_url_link'>" . 'last' . "</a></li> "; // Goto first page
				}
			}
		} else {
			echo "No articles was found.";
		}
	}
}
?>
