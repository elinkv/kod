<?php
include_once 'config/db.php';
class Links {

	function get_10_newest_articles() {

		$this->db_connection = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

		if (!$this->db_connection->set_charset("utf8")) {
			$this->errors[] = $this->db_connection->error;
		}

		$sql = "SELECT article_title, part_1, part_2, part_3, article_image_name, article_image_alt, added, page_link, meta_page_title, meta_page_description, meta_page_keywords, category, sub_category
	  FROM articles ORDER BY added DESC LIMIT 10";
	  
		$query = $this->db_connection->query($sql);
		while ($row = $query->fetch_assoc()) {

		$article_title = $row['article_title'];
		$article_category = $row['category'];
		$article_sub_category = $row['sub_category'];
			$page_link = $row['page_link'];

			echo "<li><a href='article.php?a=" . $page_link . "'>" . $article_title . "</a></li>";
		}
	}

	function get_10_random_articles() {

		$this->db_connection = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

		if (!$this->db_connection->set_charset("utf8")) {
			$this->errors[] = $this->db_connection->error;
		}

		$sql = "SELECT article_title, part_1, part_2, part_3, article_image_name, article_image_alt, added, page_link, meta_page_title, meta_page_description, meta_page_keywords, category, sub_category
	  FROM articles ORDER BY RAND() LIMIT 10";
	  
		$query = $this->db_connection->query($sql);

		while ($row = $query->fetch_assoc()) {

			$article_title = $row['article_title'];
			$article_category = $row['category'];
			$article_sub_category = $row['sub_category'];
			$page_link = $row['page_link'];
			
			echo "<li><a href='article.php?a=" . $page_link . "'>" . $article_title . "</a></li>";
		}
	}
}
?>
