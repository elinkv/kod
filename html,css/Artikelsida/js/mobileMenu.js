function mobileMenu() {

  let mobileMenu = document.getElementById("mobile");
  if (mobileMenu.style.display == "none" || mobileMenu.style.display == null || mobileMenu.style.display == "") {
    mobileMenu.style.display = "block";
  } else if (mobileMenu.style.display == "block") {
    mobileMenu.style.display = "none";
  }
}