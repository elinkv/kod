function showSearchBox() {

  var search_container = document.getElementById("search_container");

  if (search_container.style.height == null || search_container.style.height == "0px" || search_container.style.height == "") {
    search_container.style.display = "block";
    search_container.style.height = "44px";
  } else {
    search_container.style.height = "0px";
    search_container.style.display = "none";
  }
}

if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
  // ....
  function colorChange() {}

  function colorChangeBack() {}

} else {
  // TODO: använd font awesome ist för .png ...
  let searchIcon = document.getElementById("search_it");

  function colorChange() {
    searchIcon.src = "img/search-o.png";
  }

  function colorChangeBack() {
    searchIcon.src = "img/search-w.png";
  }
}

function get_search() {

  let value = document.getElementById("search_input").value;
  let ready = value.replace(/ /g, "_");
  window.location.href = "search.php?ts=" + ready;
}

function searchKeyPress(e) {

  e = e || window.event;
  if (e.keyCode == 13) {
    document.getElementById('search_button').click();
    return false;
  }
  return true;
}