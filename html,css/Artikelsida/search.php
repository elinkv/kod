<?php

include_once 'classes/Links.php';
include_once 'classes/Search.php';
include_once 'classes/paginationCategories.php';
include_once 'classes/Links.php';

$links = new Links();
$search = new Search();
$pagination = new paginationCategories();
$links = new Links();

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title></title>
  <link rel="stylesheet" href="css/style.css">
  <meta content="width=device-width, initial-scale=1.0, user-scalable=0" name="viewport">
</head>
<body>
  <header>
    <nav id="desktop">
      <div>
        <ul id="nav_desktop">
          <li><a href="index.php" id="logo_text">Name</a></li>
          <li><a href="index.php">Home</a></li>
          <li><a href="category.php">All articles</a></li>
          <li><a href="category.php?c=Economy">Economy</a></li>
          <li><a href="category.php?c=Other">Other</a></li>
        </ul>
        <ul>
          <li onclick="showSearchBox(); return 0;" onmouseover="colorChange()" onmouseleave="colorChangeBack()" id="srch_n"><img src="img/search-w.png" id="search_it"></li>
          <li id="menu_icon" onclick="mobileMenu(); return 0;"><img src="img/menu.png"></li>
        </ul>
      </div>
    </nav>
    <div id="important_message"></div>
    <div id="search_container">
      <div id="search_it">
        <form class="search_form">
          <input type="text" name="" id="search_input" onkeypress="return searchKeyPress(event);">
          <button type="button" name="ts" onclick="get_search();" id="search_button">Search</button>
        </form>
      </div>
    </div>
    <nav id="mobile">
      <div>
        <ul>
          <li><a href="index.php">Home</a></li>
          <li><a href="category.php">All articles</a></li>
          <li><a href="category.php?c=Economy">Economy</a></li>
          <li><a href="category.php?c=Other">Other</a></li>
        </ul>
      </div>
    </nav>
    <div id="banner_ad_container">
      <div></div>
    </div>
  </header>
  <main id="list">
    <div class="search_container">
      <div class="search_headers">
        <h1>Search</h1>
        <h4>Searching for: <?php echo $search->get_search_text(); ?></h4>
      </div>
      <div class="list_items_container">
        <?php $search->output_all_articles_based_on_search_text();?>
        <div id="pagination">
        <ul>
          <?php $search->outputPagination();?>
        </ul>
        </div>
     </div>
    </div>
    <aside>
      <h3>Newest articles</h3>
      <div class="subject_l_container">
        <div></div>
      </div>
      <ul>
        <?php $links->get_10_newest_articles();?>
      </ul>
      <h3 class="margin-t-10">Other articles</h3>
      <div class="subject_l_container">
        <div></div>
      </div>
      <ul>
        <?php $links->get_10_random_articles();?>
      </ul>
    </aside>
  </main>
  <footer>
    <ul>
      <li><a href="about.php">About us</a></li>
      <li><a href="contact.php">Contact</a></li>
      <li><a href="terms-of-use.php">Terms of use</a></li>
      <li><a href="privacy-policy.php">Privacy Policy</a></li>
    </ul>
    <p>© Copyright 2017. All Rights Reserved.</p>
  </footer>

  <noscript>This website works best with JavaScript enabled</noscript>
  <script type="text/javascript" src="js/search.js"></script>
  <script type="text/javascript" src="js/mobileMenu.js"></script>

</body>
</html>

