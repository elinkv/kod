<?php
include_once 'classes/indexArticles.php';

$article = new indexArticles();
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="description" content="articles">
  <title>titel</title>
  <link rel="stylesheet" href="css/style.css">
  <meta content="width=device-width, initial-scale=1.0, user-scalable=0" name="viewport">
</head>
<body>
  <header>
    <nav id="desktop">
      <div>
        <ul id="nav_desktop">
          <li><a href="index.php" id="logo_text">Name</a></li>
          <li><a href="index.php">Home</a></li>
          <li><a href="category.php">All articles</a></li>
          <li><a href="category.php?c=Economy">Economy</a></li>
          <li><a href="category.php?c=Other">Other</a></li>
        </ul>
        <ul>
        <li onclick="showSearchBox(); return 0;" onmouseover="colorChange()" onmouseleave="colorChangeBack()" id="srch_n"><img src="img/search-w.png" id="search_it"></li>
        <li id="menu_icon" onclick="mobileMenu(); return 0;"><img src="img/menu.png"></li>
        </ul>
      </div>
    </nav>
    <div id="important_message"></div>
    <div id="search_container">
      <div id="search_it">
        <form class="search_form">
          <input type="text" name="" id="search_input" onkeypress="return searchKeyPress(event);">
          <button type="button" name="ts" onclick="get_search();" id="search_button">Search</button>
        </form>
      </div>
    </div>
    <nav id="mobile">
      <div>
        <ul>
          <li><a href="index.php">Home</a></li>
          <li><a href="category.php">All articles</a></li>
          <li><a href="category.php?c=Economy">Economy</a></li>
          <li><a href="category.php?c=Other">Other</a></li>
        </ul>
      </div>
    </nav>
    <div id="banner_ad_container">
      <div></div>
    </div>
  </header>
  <div id="index_container">
    <div id="three_newest_container">
      <a href="<?php echo 'article.php?a=' . $article->newest_1_page_link . ''; ?>">
        <div id="big_image"><img id="b_image" src="<?php echo '' . $article->newest_1_article_image_name . ''; ?>" alt="<?php echo $article->newest_1_article_image_alt; ?>">
          <div id="b_img_text">
            <h2><?php echo $article->newest_1_article_title; ?></h2>
          </div>
        </div>
      </a>
      <div id="small_img_container">
        <a href="<?php echo 'article.php?a=' . $article->newest_2_page_link . ''; ?>">
          <div class="holder">
            <div class="s_img_text">
              <h3><?php echo $article->newest_2_article_title; ?></h3>
            </div>
            <img src="<?php echo '' . $article->newest_2_article_image_name . ''; ?>" alt="<?php echo $article->newest_2_article_image_alt; ?>">
          </div>
        </a>
        <a href="<?php echo 'article.php?a=' . $article->newest_3_page_link . ''; ?>">
          <div class="holder disable">
            <div class="s_img_text">
              <h3><?php echo $article->newest_3_article_title; ?></h3>
            </div>
            <img src="<?php echo '' . $article->newest_3_article_image_name . ''; ?>" alt="<?php echo $article->newest_3_article_image_alt; ?>">
          </div>
        </a>
      </div>
    </div>
  </div>
  <footer>
    <ul>
      <li><a href="about.php">About us</a></li>
      <li><a href="contact.php">Contact</a></li>
      <li><a href="terms-of-use.php">Terms of use</a></li>
      <li><a href="privacy-policy.php">Privacy Policy</a></li>
    </ul>
    <p>© Copyright 2017. All Rights Reserved.</p>
  </footer>

  <noscript>This website works best with JavaScript enabled</noscript>
  <script type="text/javascript" src="js/search.js"></script>
  <script type="text/javascript" src="js/mobileMenu.js"></script>
</body>
</html>
