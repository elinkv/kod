-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Värd: 127.0.0.1
-- Tid vid skapande: 20 sep 2018 kl 19:18
-- Serverversion: 10.1.31-MariaDB
-- PHP-version: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Databas: `articles`
--

-- --------------------------------------------------------

--
-- Tabellstruktur `articles`
--

CREATE TABLE `articles` (
  `id` int(11) NOT NULL,
  `article_title` longtext COLLATE utf8_swedish_ci NOT NULL,
  `part_1` longtext COLLATE utf8_swedish_ci NOT NULL,
  `part_2` longtext COLLATE utf8_swedish_ci NOT NULL,
  `part_3` longtext COLLATE utf8_swedish_ci NOT NULL,
  `part_4` longtext COLLATE utf8_swedish_ci NOT NULL,
  `article_image_name` longtext COLLATE utf8_swedish_ci NOT NULL,
  `article_image_alt` longtext COLLATE utf8_swedish_ci NOT NULL,
  `added` longtext COLLATE utf8_swedish_ci NOT NULL,
  `page_link` longtext COLLATE utf8_swedish_ci NOT NULL COMMENT 'article_title with - for spaces. for example: article-number-one',
  `meta_page_title` longtext COLLATE utf8_swedish_ci NOT NULL,
  `meta_page_description` longtext COLLATE utf8_swedish_ci NOT NULL,
  `meta_page_keywords` longtext COLLATE utf8_swedish_ci NOT NULL,
  `category` longtext COLLATE utf8_swedish_ci NOT NULL,
  `sub_category` longtext COLLATE utf8_swedish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

--
-- Dumpning av Data i tabell `articles`
--

INSERT INTO `articles` (`id`, `article_title`, `part_1`, `part_2`, `part_3`, `part_4`, `article_image_name`, `article_image_alt`, `added`, `page_link`, `meta_page_title`, `meta_page_description`, `meta_page_keywords`, `category`, `sub_category`) VALUES
(1, 'Article 1', '<p>Lorem ipsum dolor sit amet, ex quo malis omnes rationibus, ei saperet feugait fastidii nec. Mea an animal fabulas posidonium, usu in discere ancillae eleifend. Ius te aliquip perpetua, usu delenit nostrum electram ad. Ius iuvaret iudicabit in, vix veri ceteros volutpat cu, duis veri repudiare mei ea.\r\n\r\nIn nam quot volumus, te his homero facilisi conceptam. Cu inani posidonium his, sed assum appareat vituperatoribus in. Ius modus evertitur gloriatur ex, quem summo at pri, te quo tale soluta voluptatum. Ea graece ornatus intellegat qui, possit mandamus pri id, eos quas illud intellegat cu. Nam at alii reque singulis. An enim ludus eum.\r\n\r\nUt eripuit commune honestatis eos, ad praesent vulputate eos. Sanctus sadipscing ne sed, fugit causae quaestio ea usu. Scripta minimum his at, te atqui possit dignissim eos. Ea eum inani antiopam, et solum doming repudiandae cum. Graece tractatos eos ei, mel mundi viris delicata ne. Etiam theophrastus ne pri, cu sonet audiam equidem nec, ei dicam interpretaris vim.\r\n\r\nMelius discere mel cu. Nam prima facete mentitum ex. Et sanctus lobortis cum, no ius moderatius posidonium. Mei id electram adversarium. Vis te elitr sanctus.\r\n\r\nMei no habeo suscipit disputationi. Atqui consectetuer ad pri. Tation timeam ut sea, vel ad omnis latine impetus, nam id sanctus voluptatibus. Tamquam rationibus vix eu. Et eam habeo essent delicatissimi, diam copiosae voluptatibus eam te.</p>', '<h3>1. Lorem ipsum</h3> \r\n\r\n<p>Lorem ipsum dolor sit amet, ex quo malis omnes rationibus, ei saperet feugait fastidii nec. Mea an animal fabulas posidonium, usu in discere ancillae eleifend. Ius te aliquip perpetua, usu delenit nostrum electram ad. Ius iuvaret iudicabit in, vix veri ceteros volutpat cu, duis veri repudiare mei ea.\r\n</p>\r\n\r\n<h3>2. lorem ipsum</h3>\r\n<p>Lorem ipsum dolor sit amet, ex quo malis omnes rationibus, ei saperet feugait fastidii nec. Mea an animal fabulas posidonium, usu in discere ancillae eleifend. Ius te aliquip perpetua, usu delenit nostrum electram ad. Ius iuvaret iudicabit in, vix veri ceteros volutpat cu, duis veri repudiare mei ea.\r\n</p>', '<h3>1. Lorem ipsum</h3> \r\n\r\n<p>Lorem ipsum dolor sit amet, ex quo malis omnes rationibus, ei saperet feugait fastidii nec. Mea an animal fabulas posidonium, usu in discere ancillae eleifend. Ius te aliquip perpetua, usu delenit nostrum electram ad. Ius iuvaret iudicabit in, vix veri ceteros volutpat cu, duis veri repudiare mei ea.\r\n</p>\r\n\r\n<h3>2. lorem ipsum</h3>\r\n<p>Lorem ipsum dolor sit amet, ex quo malis omnes rationibus, ei saperet feugait fastidii nec. Mea an animal fabulas posidonium, usu in discere ancillae eleifend. Ius te aliquip perpetua, usu delenit nostrum electram ad. Ius iuvaret iudicabit in, vix veri ceteros volutpat cu, duis veri repudiare mei ea.\r\n</p>', '', 'article_images/b1.jpg', 'img_alt', '2017-12-27 01:14:51', 'article-one', 'page_title', 'description', 'keywords', 'Economy', 'a'),
(2, 'Article 2', '<p>Lorem ipsum dolor sit amet, ex quo malis omnes rationibus, ei saperet feugait fastidii nec. Mea an animal fabulas posidonium, usu in discere ancillae eleifend. Ius te aliquip perpetua, usu delenit nostrum electram ad. Ius iuvaret iudicabit in, vix veri ceteros volutpat cu, duis veri repudiare mei ea.\r\n\r\nIn nam quot volumus, te his homero facilisi conceptam. Cu inani posidonium his, sed assum appareat vituperatoribus in. Ius modus evertitur gloriatur ex, quem summo at pri, te quo tale soluta voluptatum. Ea graece ornatus intellegat qui, possit mandamus pri id, eos quas illud intellegat cu. Nam at alii reque singulis. An enim ludus eum.\r\n\r\nUt eripuit commune honestatis eos, ad praesent vulputate eos. Sanctus sadipscing ne sed, fugit causae quaestio ea usu. Scripta minimum his at, te atqui possit dignissim eos. Ea eum inani antiopam, et solum doming repudiandae cum. Graece tractatos eos ei, mel mundi viris delicata ne. Etiam theophrastus ne pri, cu sonet audiam equidem nec, ei dicam interpretaris vim.\r\n\r\nMelius discere mel cu. Nam prima facete mentitum ex. Et sanctus lobortis cum, no ius moderatius posidonium. Mei id electram adversarium. Vis te elitr sanctus.\r\n\r\nMei no habeo suscipit disputationi. Atqui consectetuer ad pri. Tation timeam ut sea, vel ad omnis latine impetus, nam id sanctus voluptatibus. Tamquam rationibus vix eu. Et eam habeo essent delicatissimi, diam copiosae voluptatibus eam te.</p>', '<h3>1. Lorem ipsum</h3> \r\n\r\n<p>Lorem ipsum dolor sit amet, ex quo malis omnes rationibus, ei saperet feugait fastidii nec. Mea an animal fabulas posidonium, usu in discere ancillae eleifend. Ius te aliquip perpetua, usu delenit nostrum electram ad. Ius iuvaret iudicabit in, vix veri ceteros volutpat cu, duis veri repudiare mei ea.\r\n</p>\r\n\r\n<h3>2. lorem ipsum</h3>\r\n<p>Lorem ipsum dolor sit amet, ex quo malis omnes rationibus, ei saperet feugait fastidii nec. Mea an animal fabulas posidonium, usu in discere ancillae eleifend. Ius te aliquip perpetua, usu delenit nostrum electram ad. Ius iuvaret iudicabit in, vix veri ceteros volutpat cu, duis veri repudiare mei ea.\r\n</p>', '<h3>1. Lorem ipsum</h3> \r\n\r\n<p>Lorem ipsum dolor sit amet, ex quo malis omnes rationibus, ei saperet feugait fastidii nec. Mea an animal fabulas posidonium, usu in discere ancillae eleifend. Ius te aliquip perpetua, usu delenit nostrum electram ad. Ius iuvaret iudicabit in, vix veri ceteros volutpat cu, duis veri repudiare mei ea.\r\n</p>\r\n\r\n<h3>2. lorem ipsum</h3>\r\n<p>Lorem ipsum dolor sit amet, ex quo malis omnes rationibus, ei saperet feugait fastidii nec. Mea an animal fabulas posidonium, usu in discere ancillae eleifend. Ius te aliquip perpetua, usu delenit nostrum electram ad. Ius iuvaret iudicabit in, vix veri ceteros volutpat cu, duis veri repudiare mei ea.\r\n</p>', '', 'article_images/b2.jpg', 'img_alt', '2018-01-12 12:42:06', 'article-two', 'page_title', 'description', 'keywords', 'Other', 'asd'),
(3, 'Article 3', '<p>Lorem ipsum dolor sit amet, ex quo malis omnes rationibus, ei saperet feugait fastidii nec. Mea an animal fabulas posidonium, usu in discere ancillae eleifend. Ius te aliquip perpetua, usu delenit nostrum electram ad. Ius iuvaret iudicabit in, vix veri ceteros volutpat cu, duis veri repudiare mei ea.\r\n\r\nIn nam quot volumus, te his homero facilisi conceptam. Cu inani posidonium his, sed assum appareat vituperatoribus in. Ius modus evertitur gloriatur ex, quem summo at pri, te quo tale soluta voluptatum. Ea graece ornatus intellegat qui, possit mandamus pri id, eos quas illud intellegat cu. Nam at alii reque singulis. An enim ludus eum.\r\n\r\nUt eripuit commune honestatis eos, ad praesent vulputate eos. Sanctus sadipscing ne sed, fugit causae quaestio ea usu. Scripta minimum his at, te atqui possit dignissim eos. Ea eum inani antiopam, et solum doming repudiandae cum. Graece tractatos eos ei, mel mundi viris delicata ne. Etiam theophrastus ne pri, cu sonet audiam equidem nec, ei dicam interpretaris vim.\r\n\r\nMelius discere mel cu. Nam prima facete mentitum ex. Et sanctus lobortis cum, no ius moderatius posidonium. Mei id electram adversarium. Vis te elitr sanctus.\r\n\r\nMei no habeo suscipit disputationi. Atqui consectetuer ad pri. Tation timeam ut sea, vel ad omnis latine impetus, nam id sanctus voluptatibus. Tamquam rationibus vix eu. Et eam habeo essent delicatissimi, diam copiosae voluptatibus eam te.</p>', '<h3>1. Lorem ipsum</h3> \r\n\r\n<p>Lorem ipsum dolor sit amet, ex quo malis omnes rationibus, ei saperet feugait fastidii nec. Mea an animal fabulas posidonium, usu in discere ancillae eleifend. Ius te aliquip perpetua, usu delenit nostrum electram ad. Ius iuvaret iudicabit in, vix veri ceteros volutpat cu, duis veri repudiare mei ea.\r\n</p>\r\n\r\n<h3>2. lorem ipsum</h3>\r\n<p>Lorem ipsum dolor sit amet, ex quo malis omnes rationibus, ei saperet feugait fastidii nec. Mea an animal fabulas posidonium, usu in discere ancillae eleifend. Ius te aliquip perpetua, usu delenit nostrum electram ad. Ius iuvaret iudicabit in, vix veri ceteros volutpat cu, duis veri repudiare mei ea.\r\n</p>', '<h3>1. Lorem ipsum</h3> \r\n\r\n<p>Lorem ipsum dolor sit amet, ex quo malis omnes rationibus, ei saperet feugait fastidii nec. Mea an animal fabulas posidonium, usu in discere ancillae eleifend. Ius te aliquip perpetua, usu delenit nostrum electram ad. Ius iuvaret iudicabit in, vix veri ceteros volutpat cu, duis veri repudiare mei ea.\r\n</p>\r\n\r\n<h3>2. lorem ipsum</h3>\r\n<p>Lorem ipsum dolor sit amet, ex quo malis omnes rationibus, ei saperet feugait fastidii nec. Mea an animal fabulas posidonium, usu in discere ancillae eleifend. Ius te aliquip perpetua, usu delenit nostrum electram ad. Ius iuvaret iudicabit in, vix veri ceteros volutpat cu, duis veri repudiare mei ea.\r\n</p>', '', 'article_images/b3.jpg', 'img_alt', '2018-01-13 18:23:36', 'article-three', 'page_title', 'description', 'keywords', 'Other', 'asd');

--
-- Index för dumpade tabeller
--

--
-- Index för tabell `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT för dumpade tabeller
--

--
-- AUTO_INCREMENT för tabell `articles`
--
ALTER TABLE `articles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
